interface Entity {
  id: number;
  name: string;
}

export interface Playlist extends Entity {
  type: 'playlist';
  /**
   * HEX color
   */
  color: string;
  favorite: boolean;
  tracks?: Track[];
}

export interface Track extends Entity {
  type: 'track';
  duration: number;
}

// type Entity = Playlist | Track;

// const p: Entity = {};

// if (p.type == 'playlist') {
//   p.tracks.length = 0
// } else {
//   p.duration;
// }

// interface Vector {
//   x: number;
//   y: number;
//   length?: number;
// }

// interface Point {
//   x: number;
//   y: number;
// }

// let v: Vector = { x: 1, y: 2, length: 123 };
// let p: Point = { x: 1, y: 2 };

// v = p;

// p = v;
