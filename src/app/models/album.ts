export interface Entity {
  id: string;
  name: string;
}

export interface Album extends Entity {
  images: AlbumImage[];
  artists: Artist[];
}

export interface Artist extends Entity {
  popularity: number;
}

export interface AlbumImage {
  url: string;
}

export interface PageObject<T> {
  items: T[];
  offset?: number;
  total?: number;
}

export interface AlbumsResponse {
  albums: PageObject<Album>;
}
