import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {
  Router,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError,
} from '@angular/router';
import { filter, map, retry, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-nav-loader',
  templateUrl: './nav-loader.component.html',
  styleUrls: ['./nav-loader.component.scss'],
})
export class NavLoaderComponent implements OnInit {
  // isLoading = new BehaviorSubject(true);
  isLoading = this.router.events.pipe(
    filter(
      (e) =>
        e instanceof NavigationStart ||
        e instanceof NavigationEnd ||
        e instanceof NavigationCancel ||
        e instanceof NavigationError
    ),
    map((e) => {

      if (e instanceof NavigationStart) {
        return true;
      } else {
        return false;
      }
    }),
    distinctUntilChanged()
  );

  constructor(private router: Router) {}

  ngOnInit(): void {}
}
