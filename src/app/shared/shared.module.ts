import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { HighlightDirective } from './highlight.directive';
import { CardComponent } from './card/card.component';

import { NavLoaderModule } from 'dist/nav-loader';

@NgModule({
  declarations: [HighlightDirective, CardComponent],
  imports: [CommonModule, FormsModule, NavLoaderModule],
  exports: [FormsModule, HighlightDirective, CardComponent, NavLoaderModule],
})
export class SharedModule {}
