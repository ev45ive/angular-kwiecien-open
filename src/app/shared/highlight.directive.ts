import {
  Directive,
  ElementRef,
  Inject,
  Input,
  OnChanges,
  OnInit,
  Renderer2,
  HostBinding,
  HostListener,
} from '@angular/core';

@Directive({
  selector: '[appHighlight]',
  // host: {
  //   '[style.border-left-color]': 'hover? color : '' ',
  //   '(mouseenter)':' mouseetner($event)'
  // },
})
export class HighlightDirective implements OnChanges, OnInit {
  @Input('appHighlight')
  color: string = '';

  // @HostBinding('style.border-left-color')
  @HostBinding('style.color')
  get activeColor() {
    return this.hover ? this.color : '';
  }

  hover: boolean;

  constructor(
    private elem: ElementRef<HTMLElement>,
    private renderer: Renderer2
  ) {}

  @HostListener('mouseenter', ['$event.x', '$event.y'])
  onMouseEnter(x: number, y: number) {
    this.hover = true;
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.hover = false;
  }

  ngOnChanges(chages) {}

  ngOnInit() {
    // this.elem.nativeElement.style.color = this.color;

    // this.renderer.setStyle(this.elem.nativeElement, 'color', this.color);

    console.log('Hello appHighlight', this.color);

    // $(this.elem).myFavPlugin()
  }

  ngOnDestory() {
    // $(this.elem).myFavPlugin('destroy')
  }
}
