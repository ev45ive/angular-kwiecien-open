import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild,
  AfterViewInit,
  ViewChildren,
  QueryList,
} from '@angular/core';
import { Playlist } from 'src/app/models';
import { NgForm, NgModel, Validators } from '@angular/forms';

@Component({
  selector: 'app-playlist-form',
  templateUrl: './playlist-form.component.html',
  styleUrls: ['./playlist-form.component.scss'],
})
export class PlaylistFormComponent implements OnInit, AfterViewInit {
  // @ViewChild('formRef')
  @ViewChild(NgForm, { read: NgForm, static: true })
  formRef: NgForm;

  @ViewChildren(NgModel)
  models: QueryList<NgModel>;

  @Input()
  playlist: Playlist;

  @Output() cancel = new EventEmitter();

  @Output() save = new EventEmitter<Playlist>();

  constructor() {}

  ngAfterViewInit(): void {
    // console.log(this.models)
    setTimeout(() => {
      this.formRef.form.get('name').setValidators([
        Validators.required
      ]);
      this.formRef.form.updateValueAndValidity();
    });
  }

  reset() {
    this.formRef.resetForm(this.playlist);
  }

  ngOnInit(): void {
    // console.log(this.formRef);
  }

  emitCancel() {
    this.cancel.emit();
  }

  submit(draft: Playlist) {
    this.save.emit({
      ...this.playlist,
      ...draft,
    });
  }
}
