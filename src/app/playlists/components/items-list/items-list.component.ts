import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
} from '@angular/core';
import { Playlist } from 'src/app/models';
import { NgIf, NgForOf, NgForOfContext } from '@angular/common';

NgIf;
NgForOf;
NgForOfContext;

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.scss'],
  // inputs:['playlists:items'],
  // changeDetection:ChangeDetectionStrategy.OnPush
})
export class ItemsListComponent implements OnInit {

  hover

  @Input('items')
  playlists: Playlist[] = [];

  @Input()
  selected: Playlist = null;

  @Output()
  selectedChange = new EventEmitter<Playlist>();

  select(playlist: Playlist) {
    this.selectedChange.emit(playlist);
  }

  constructor() {}

  ngOnInit(): void {}

  trackFn(index:number, item:Playlist){
    return item.id
  }
}
