import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaylistsRoutingModule } from './playlists-routing.module';
import { PlaylistsViewComponent } from './views/playlists-view/playlists-view.component';
import { ItemsListComponent } from './components/items-list/items-list.component';
import { ListItemComponent } from './components/list-item/list-item.component';
import { PlaylistDetailsComponent } from './components/playlist-details/playlist-details.component';
import { PlaylistFormComponent } from './components/playlist-form/playlist-form.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import * as fromPlaylists from './reducers';
import * as fromPlaylist from './reducers/playlist.reducer';
import { EffectsModule } from '@ngrx/effects';
import { PlaylistEffects } from './effects/playlist.effects';
import { ReduxPlaylistsViewComponent } from './views/redux-playlists-view/redux-playlists-view.component';
import { PlaylistsListComponent } from './containers/playlists-list/playlists-list.component';
import { SelectedPlaylistComponent } from './containers/selected-playlist/selected-playlist.component';
import { PlaylistEditorComponent } from './containers/playlist-editor/playlist-editor.component';

@NgModule({
  declarations: [
    PlaylistsViewComponent,
    ItemsListComponent,
    ListItemComponent,
    PlaylistDetailsComponent,
    PlaylistFormComponent,
    ReduxPlaylistsViewComponent,
    PlaylistsListComponent,
    SelectedPlaylistComponent,
    PlaylistEditorComponent,
  ],
  imports: [
    CommonModule,
    PlaylistsRoutingModule,
    SharedModule,
    StoreModule.forFeature(
      fromPlaylists.playlistsFeatureKey,
      fromPlaylists.reducers,
      { metaReducers: fromPlaylists.metaReducers }
    ),
    EffectsModule.forFeature([PlaylistEffects]),
  ],
  // exports:[
  //   PlaylistsViewComponent
  // ]
})
export class PlaylistsModule {}
