import { Action, createReducer, on } from '@ngrx/store';
import * as PlaylistActions from '../actions/playlist.actions';
import { Playlist } from 'src/app/models';

export const playlistFeatureKey = 'playlist';

export interface State {
  // items: Playlist[]
  // entities: { [key: number]: Playlist };
  entities: Record<Playlist['id'], Playlist>;
  list: Playlist['id'][];
  favorite: Playlist['id'][];
  selectedId: Playlist['id'] | null;
  loading: boolean;
  error: any;
}

export const initialState: State = {
  entities: {},
  list: [],
  favorite: [],
  selectedId: null,
  loading: false,
  error: null,
};

export const reducer = createReducer(
  initialState,

  on(PlaylistActions.updatePlaylist, (state, action) => ({
    ...state,
    entities: { ...state.entities, [action.draft.id]: action.draft },
  })),

  on(PlaylistActions.selectPlaylist, (state,action)=>({
    ...state,
    selectedId: action.id
  })),

  on(PlaylistActions.loadPlaylists, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),

  on(PlaylistActions.loadPlaylistsSuccess, (state, action) => ({
    ...state,
    loading: false,
    list: action.data.map((p) => p.id),
    entities: action.data.reduce(
      (entities, item) => ({
        ...entities,
        [item.id]: item,
      }),
      state.entities
    ),
  })),

  on(PlaylistActions.loadPlaylistsFailure, (state, action) => ({
    ...state,
    loading: false,
    error: action.error,
  }))
);
