import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import * as fromPlaylist from './playlist.reducer';
import { environment } from 'src/environments/environment';

export const playlistsFeatureKey = 'playlists';

export interface State {

  [fromPlaylist.playlistFeatureKey]: fromPlaylist.State;
}

export const reducers: ActionReducerMap<State> = {

  [fromPlaylist.playlistFeatureKey]: fromPlaylist.reducer,
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
