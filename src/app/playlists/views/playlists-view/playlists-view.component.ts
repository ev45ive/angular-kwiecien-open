import { Component, OnInit } from '@angular/core';
import { Playlist } from 'src/app/models';

@Component({
  selector: 'app-playlists-view',
  templateUrl: './playlists-view.component.html',
  styleUrls: ['./playlists-view.component.scss'],
})
export class PlaylistsViewComponent implements OnInit {
  mode: 'show' | 'edit' = 'show';

  edit() {
    this.mode = 'edit';
  }

  cancel() {
    this.mode = 'show';
  }

  save(draft: Playlist) {
    const index = this.playlists.findIndex((p) => p.id == draft.id);
    if (index !== -1) {
      this.playlists.splice(index, 1, draft);
    }
    this.selected = draft
    this.mode = 'show'
  }

  constructor() {}

  ngOnInit(): void {}

  playlists: Playlist[] = [
    {
      id: 123,
      name: 'Angular HITS',
      color: '#ff00ff',
      favorite: true,
      type: 'playlist',
    },
    {
      id: 234,
      name: 'Angular Top20',
      color: '#ffff00',
      favorite: true,
      type: 'playlist',
    },
    {
      id: 345,
      name: 'Best of Angular',
      color: '#00ffff',
      favorite: false,
      type: 'playlist',
    },
  ];

  selected: Playlist = this.playlists[2];
}
