import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReduxPlaylistsViewComponent } from './redux-playlists-view.component';
import { Store, StoreModule } from '@ngrx/store';

describe('ReduxPlaylistsViewComponent', () => {
  let component: ReduxPlaylistsViewComponent;
  let fixture: ComponentFixture<ReduxPlaylistsViewComponent>;
  let store: Store<any>;

  beforeEach(async() => {
    TestBed.configureTestingModule({
      imports: [ StoreModule.forRoot({}) ],
      declarations: [ ReduxPlaylistsViewComponent ]
    });

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReduxPlaylistsViewComponent);
    component = fixture.componentInstance;
    store = TestBed.get<Store>(Store);

    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
