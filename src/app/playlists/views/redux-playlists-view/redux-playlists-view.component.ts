import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromStore from '../../../reducers';
import {
  selectPlaylists,
  getSelectedPlaylist,
} from '../../selectors/playlist.selectors';
import { selectPlaylist } from '../../actions/playlist.actions';
import { Playlist } from 'src/app/models';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-redux-playlists-view',
  templateUrl: './redux-playlists-view.component.html',
  styleUrls: ['./redux-playlists-view.component.css'],
})
export class ReduxPlaylistsViewComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
