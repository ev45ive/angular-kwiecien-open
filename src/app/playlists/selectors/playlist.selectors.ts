import { createFeatureSelector, createSelector, select } from '@ngrx/store';
import * as fromPlaylists from '../reducers';
import * as fromPlaylist from '../reducers/playlist.reducer';
import { pipe } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Playlist } from 'src/app/models';

export const selectPlaylistsState = createFeatureSelector<fromPlaylists.State>(
  fromPlaylists.playlistsFeatureKey
);

export const selectPlaylistState = createFeatureSelector<fromPlaylist.State>(
  fromPlaylist.playlistFeatureKey
);

export const playlistsPlaylistState = createSelector(
  selectPlaylistsState,
  selectPlaylistState
);

export const selectPlaylists = createSelector(
  playlistsPlaylistState,
  createSelector(
    (state) => state.list,
    (state) => state.entities,
    (items, entities) => items.map((id) => entities[id])
  )
);

export const getSelectedPlaylist = createSelector(
  playlistsPlaylistState,
  createSelector(
    (state) => state.selectedId,
    (state) => state.entities,
    (id, e) => e[id]
  )
);


export const getPlaylistById = createSelector(
  playlistsPlaylistState,
  createSelector(
    (state) => state.entities,
    (entities,{id}) => entities[id]
  )
);

export const selectExisting = (
  mapFn: (state: unknown, props: unknown) => unknown,
  props?: unknown
) =>
  pipe(
    select(mapFn, props),
    filter((v) => v !== undefined && v !== null)
  );

// selectPlaylists.release() // remove memorized result
// selectPlaylists.projector

// selectPlaylists({
//   playlists:{
//     entities:{}, list:[]
//   }
// }) == playlists

// entities:{
//   playlists:{ [[id]: playlist]},
//   tracks:{ [[id]: Track]}
//   aritsts:{ [[id]: Artist]}
// }

// playlist:{
//   tracks:[2,5432,34,547,]
// }

// track{
//   artists:[2,5432,34,547,]
// }

// [
//   {
//     ...state,
//     name:'playlist',
//     tracks:[
//       {
//         ...tracks,
//         artists:{
//           ...artists,
//           Zenek:{
//             ...zenek
//             online:true
//           }
//         }
//       },
//       {
//         artists:{
//           Zenek
//         }
//       }
//     ]
//   }
// ]

// <app-playlists>          // OnPush
//   <app-playlist_items>   // OnPush
//     <app-playlist>       // OnPush
//       <app-tracks>       // OnPush
//         <app-track>      // OnPush
//            <app-artist>  // OnPush
