import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReduxPlaylistsViewComponent } from './views/redux-playlists-view/redux-playlists-view.component';
import { SelectedPlaylistComponent } from './containers/selected-playlist/selected-playlist.component';
import { PlaylistEditorComponent } from './containers/playlist-editor/playlist-editor.component';
import { PlaylistsListComponent } from './containers/playlists-list/playlists-list.component';
import { PlaylistResolverService } from './services/playlist-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: ReduxPlaylistsViewComponent,
    children: [
      {
        path: '',
        children: [
          {
            path: '',
            component: PlaylistsListComponent,
            outlet: 'list',
          },
        ],
      },
      {
        path: ':playlist_id',
        resolve: {
          playlist: PlaylistResolverService,
        },
        children: [
          {
            path: '',
            component: PlaylistsListComponent,
            outlet: 'list',
          },
          {
            path: '',
            component: SelectedPlaylistComponent,
            children: [],
          },
          {
            path: 'edit',
            component: PlaylistEditorComponent,
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlaylistsRoutingModule {}
// {
//   path: 'playlists',
//   children: [
//     {
//       path: '',
//       component: ReduxPlaylistsViewComponent,
//     },
//     {
//       path: ':playlist_id',
//       component: ReduxPlaylistsViewComponent,
//     },
//   ],
// },
