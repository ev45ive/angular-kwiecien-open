import { createAction, props } from '@ngrx/store';
import { Playlist } from 'src/app/models';

export const loadPlaylists = createAction('[Playlist] Load Playlists');

export const loadPlaylistsSuccess = createAction(
  '[Playlist] Load Playlists Success',
  props<{ data: Playlist[] }>()
);

export const loadPlaylistsFailure = createAction(
  '[Playlist] Load Playlists Failure',
  props<{ error: any }>()
);

export const updatePlaylist = createAction(
  '[Playlist] Update Playlist',
  props<{ draft: Playlist }>()
);

export const removePlaylist = createAction(
  '[Playlist] Remove Playlist',
  props<{ id: Playlist['id'] }>()
);

export const selectPlaylist = createAction(
  '[Playlist] Select Playlist',
  props<{ id: Playlist['id'] }>()
);
