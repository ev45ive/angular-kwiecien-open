import { Injectable } from '@angular/core';
import {
  Actions,
  createEffect,
  ofType,
  ROOT_EFFECTS_INIT,
} from '@ngrx/effects';
import { catchError, map, concatMap, mapTo } from 'rxjs/operators';
import { EMPTY, of } from 'rxjs';

import * as PlaylistActions from '../actions/playlist.actions';
import { Action, createAction } from '@ngrx/store';

const playlistEffectsInit = createAction('[PlaylistEffects]: Init')

@Injectable()
export class PlaylistEffects {
  constructor(private actions$: Actions) {}

  ngrxOnInitEffects(): Action {
    return playlistEffectsInit();
  }

  preloadPlaylists$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(playlistEffectsInit),
      map((action) =>
        PlaylistActions.loadPlaylistsSuccess({
          data: [
            {
              id: 123,
              name: 'Angular HITS',
              color: '#ff00ff',
              favorite: true,
              type: 'playlist',
            },
            {
              id: 234,
              name: 'Angular Top20',
              color: '#ffff00',
              favorite: true,
              type: 'playlist',
            },
            {
              id: 345,
              name: 'Best of Angular',
              color: '#00ffff',
              favorite: false,
              type: 'playlist',
            },
          ],
        })
      )
    );
  });

  loadPlaylists$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PlaylistActions.loadPlaylists),
      concatMap(() =>
        /** An EMPTY observable only emits completion. Replace with your own observable API request */
        EMPTY.pipe(
          map((data) => PlaylistActions.loadPlaylistsSuccess({ data })),
          catchError((error) =>
            of(PlaylistActions.loadPlaylistsFailure({ error }))
          )
        )
      )
    );
  });
}
