import { TestBed } from '@angular/core/testing';

import { PlaylistResolverService } from './playlist-resolver.service';

describe('PlaylistResolverService', () => {
  let service: PlaylistResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PlaylistResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
