import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { Playlist } from 'src/app/models';
import { Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';
import * as fromStore from '../../reducers';
import { getPlaylistById } from '../selectors/playlist.selectors';
import { tap, take, delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class PlaylistResolverService implements Resolve<Playlist | null> {
  constructor(private store: Store<fromStore.State>) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Playlist | Observable<Playlist> | Promise<Playlist> {
    //
    const id = route.paramMap.get('playlist_id');
    if (!id) {
      return of(null);
    }
    return (
      this.store
        .select(getPlaylistById, { id })
        // Complete to load component
        .pipe(delay(3000), take(1))
    );
  }
}
