import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromStore from '../../../reducers';
import {
  getSelectedPlaylist,
  getPlaylistById,
} from '../../selectors/playlist.selectors';
import { ActivatedRoute, Router } from '@angular/router';
import { Playlist } from 'src/app/models';
import { updatePlaylist } from '../../actions/playlist.actions';
import { map, filter, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-playlist-editor',
  templateUrl: './playlist-editor.component.html',
  styleUrls: ['./playlist-editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlaylistEditorComponent implements OnInit {
  // selected = this.store.select(getSelectedPlaylist);

  // selected = this.route.paramMap.pipe(
  //   map((paramMap) => +paramMap.get('playlist_id')),
  //   filter((id) => !!id),
  //   switchMap((id) => this.store.select( getPlaylistById, { id }))
  // );

  selected = this.route.data.pipe(map<any, Playlist>((d) => d['playlist']));

  constructor(
    private store: Store<fromStore.State>,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {}

  cancel() {
    this.router.navigate(['..'], {
      relativeTo: this.route,
    });
  }

  save(draft: Playlist) {
    this.store.dispatch(updatePlaylist({ draft }));

    this.router.navigate(['..'], {
      relativeTo: this.route,
    });
  }
}
