import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromStore from '../../../reducers';
import {
  selectPlaylists,
  getSelectedPlaylist,
  getPlaylistById,
} from '../../selectors/playlist.selectors';
import { ActivatedRoute, Router } from '@angular/router';
import { Playlist } from 'src/app/models';
import { filter, map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-playlists-list',
  templateUrl: './playlists-list.component.html',
  styleUrls: ['./playlists-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlaylistsListComponent implements OnInit {
  playlists = this.store.select(selectPlaylists);

  selected = this.route.paramMap.pipe(
    map((paramMap) => +paramMap.get('playlist_id')),
    filter((id) => !!id),
    switchMap((id) => this.store.select(getPlaylistById, { id }))
  );

  constructor(
    private store: Store<fromStore.State>,
    private router: Router,
    private route: ActivatedRoute
  ) {}
  ngOnInit(): void {}

  select({ id }: Playlist) {
    this.router.navigate([id], {
      relativeTo: this.route.parent.snapshot.paramMap.get('playlist_id')
        ? this.route.parent.parent
        : this.route,
    });
  }
}
