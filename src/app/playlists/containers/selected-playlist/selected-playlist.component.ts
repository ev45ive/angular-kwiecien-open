import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromStore from '../../../reducers';
import {
  getSelectedPlaylist,
  getPlaylistById,
} from '../../selectors/playlist.selectors';
import { Router, ActivatedRoute } from '@angular/router';
import { selectPlaylist } from '../../actions/playlist.actions';
import { map, switchMap, filter } from 'rxjs/operators';
import { Playlist } from 'src/app/models';

@Component({
  selector: 'app-selected-playlist',
  templateUrl: './selected-playlist.component.html',
  styleUrls: ['./selected-playlist.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectedPlaylistComponent implements OnInit {
  // selected = this.store.select(getSelectedPlaylist);

  // selected = this.route.paramMap.pipe(
  //   map((paramMap) => +paramMap.get('playlist_id')),
  //   filter((id) => !!id),
  //   switchMap((id) => this.store.select(getPlaylistById, { id }))
  // );
  selected = this.route.data.pipe(map<any, Playlist>((d) => d['playlist']));

  constructor(
    private store: Store<fromStore.State>,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    // this.route.paramMap
    //   .pipe(map((paramMap) => +paramMap.get('playlist_id')))
    //   .subscribe((id) => this.store.dispatch(selectPlaylist({ id })));
  }

  edit() {
    this.router.navigate(['edit'], {
      relativeTo: this.route,
    });
  }

  cancel() {
    this.router.navigate(['../../'], {
      relativeTo: this.route,
    });
  }
}
