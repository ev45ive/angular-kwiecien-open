import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectedPlaylistComponent } from './selected-playlist.component';
import { Store, StoreModule } from '@ngrx/store';

describe('SelectedPlaylistComponent', () => {
  let component: SelectedPlaylistComponent;
  let fixture: ComponentFixture<SelectedPlaylistComponent>;
  let store: Store<any>;

  beforeEach(async() => {
    TestBed.configureTestingModule({
      imports: [ StoreModule.forRoot({}) ],
      declarations: [ SelectedPlaylistComponent ]
    });

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectedPlaylistComponent);
    component = fixture.componentInstance;
    store = TestBed.get<Store>(Store);

    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
