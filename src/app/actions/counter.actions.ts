import { createAction, props } from '@ngrx/store';

export const incrementCounter = createAction(
  '[Counter] Increment Counter',
  (payload = 1) => ({ payload })
  // props<{payload:number}>(),
);

export const decrementCounter = createAction(
  '[Counter] Decrement Counter',
  (payload = 1) => ({ payload })
);

export const resetCounter = createAction(
  '[Counter] Reset Counter',
  (payload = 0) => ({ payload })
);

export const loadCounters = createAction('[Counter] Load Counters');

export const loadCountersSuccess = createAction(
  '[Counter] Load Counters Success',
  props<{ data: any }>()
);

export const loadCountersFailure = createAction(
  '[Counter] Load Counters Failure',
  props<{ error: any }>()
);
