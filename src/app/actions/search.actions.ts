import { createAction, props } from '@ngrx/store';
import { Album } from '../models/album';

export const AlbumSearch = createAction(
  '[Search] Album Search',
  props<{ query: string }>()
);

export const AlbumSearchSuccess = createAction(
  '[Search] Album Search Success',
  props<{ results: Album[] }>()
);

export const AlbumSearchFailure = createAction(
  '[Search] Album Search Failure',
  props<{ error: any }>()
);
