import { createFeatureSelector, createSelector } from '@ngrx/store';
import { musicSearchFeatureKey, State } from '../reducers/music-search.reducer';

export const searchFeatureSelector = createFeatureSelector<State>(
  musicSearchFeatureKey
);

export const albumSearchResultsSelector = createSelector(
  searchFeatureSelector,
  (state) => state.results
);

export const albumSearchQuerySelector = createSelector(
  searchFeatureSelector,
  (state) => state.query
);
