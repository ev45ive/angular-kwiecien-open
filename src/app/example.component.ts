import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-example',
  template: `
    <p>
      example works!
    </p>
  `,
  styles: [
    `
      :host {
        display: block;
      }
    `,
  ],
})
export class ExampleComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
