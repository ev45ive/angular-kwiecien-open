import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from 'src/app/reducers';
import {
  incrementCounter,
  decrementCounter,
  resetCounter,
} from 'src/app/actions/counter.actions';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css'],
})
export class CounterComponent implements OnInit {
  constructor(private store: Store<State>) {}

  counterValue = this.store.select<number>((state) => state.counter.value);

  ngOnInit(): void {
  }

  inc() {
    this.store.dispatch(incrementCounter());
  }

  dec() {
    this.store.dispatch(decrementCounter());
  }

  reset() {
    this.store.dispatch(resetCounter());
  }
}
