import { Action, createReducer, on } from '@ngrx/store';
import {
  incrementCounter,
  decrementCounter,
  resetCounter,
} from '../actions/counter.actions';

export const counterFeatureKey = 'counter';

export interface State {
  value: number;
}

export const initialState: State = {
  value: 0,
};

export const reducer = createReducer(
  initialState,

  on(incrementCounter, (state, action) => ({
    ...state,
    value: state.value + action.payload,
  })),

  on(decrementCounter, (state, action) => ({
    ...state,
    value: state.value - action.payload,
  })),

  on(resetCounter, (state, action) => ({
    ...state,
    value: action.payload,
  }))
);
