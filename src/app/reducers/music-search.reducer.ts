import { Action, createReducer, on } from '@ngrx/store';
import { Album } from '../models/album';
import {
  AlbumSearch,
  AlbumSearchSuccess,
  AlbumSearchFailure,
} from '../actions/search.actions';

export const musicSearchFeatureKey = 'musicSearch';

export interface State {
  results: Album[];
  query: string;
  error: any;
}

export const initialState: State = {
  results: [],
  query: '',
  error: null,
};

export const reducer = createReducer(
  initialState,

  on(AlbumSearch, (state, action) => ({
    ...state,
    query: action.query,
    error: null,
  })),

  on(AlbumSearchSuccess, (state, action) => ({
    ...state,
    results: action.results,
  })),

  on(AlbumSearchFailure, (state, action) => ({
    ...state,
    error: action.error,
  }))
);
