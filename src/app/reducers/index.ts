import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer,
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromCounter from './counter.reducer';
import * as fromMusicSearch from './music-search.reducer';

export interface State {
  [fromCounter.counterFeatureKey]: fromCounter.State;
  [fromMusicSearch.musicSearchFeatureKey]: fromMusicSearch.State;
}

export const reducers: ActionReducerMap<State> = {
  [fromCounter.counterFeatureKey]: fromCounter.reducer,
  [fromMusicSearch.musicSearchFeatureKey]: fromMusicSearch.reducer,
};

export const metaReducers: MetaReducer<State>[] = !environment.production
  ? []
  : [];
