import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OAuthModule } from 'angular-oauth2-oidc';
import { AuthService, AuthConfig } from './auth.service';
import { environment } from 'src/environments/environment';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptorService } from './auth-interceptor.service';

@NgModule({
  declarations: [],
  imports: [
    // OAuthModule.forRoot({
    //   resourceServer: {
    //     sendAccessToken: false,
    //     allowedUrls: [],
    //   },
    // }),
  ],
  providers: [AuthInterceptorService, ,],
})
export class SecurityModule {
  constructor(private auth: AuthService) {
    this.auth.getToken();
  }

  static forRoot(
    config: AuthConfig,
    extras = {
      interceptor: false,
      authTokenUrls: [],
    }
  ): ModuleWithProviders {
    return {
      ngModule: SecurityModule,
      providers: [
        {
          provide: AuthConfig,
          useValue: config,
        },
        extras.interceptor
          ? [
              {
                provide: HTTP_INTERCEPTORS,
                // useClass: AuthInterceptorService
                useExisting: AuthInterceptorService,
                multi: true,
              },
            ]
          : [],
      ],
    };
  }
}
