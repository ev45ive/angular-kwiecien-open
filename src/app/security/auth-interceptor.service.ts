import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError, map, mergeMap, exhaustMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthInterceptorService implements HttpInterceptor {
  constructor(private auth: AuthService) {}

  // Chain of Responsibilty
  // objA.next = objB
  // objA.handle(req)

  intercept(
    req: HttpRequest<any>, // Immutable Request Object
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const shouldAuthorize = req.url.includes('https://api.spotify.com/');

    const finalRequest = shouldAuthorize ? this.authorizeRequest(req) : req;

    return next.handle(finalRequest).pipe(
      catchError((err, caught) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status !== 401) {
            return throwError(new Error(err.error.error.message));
          }
          if (shouldAuthorize) {
            return this.auth
              .refreshToken()
              .pipe(exhaustMap(() => next.handle(this.authorizeRequest(req))));
          }
        }
        return throwError(new Error('Unexpected Error'));
      })
    );
  }

  private authorizeRequest(req: HttpRequest<any>) {
    return req.clone({
      setHeaders: {
        Authorization: 'Bearer ' + this.auth.getToken(),
      },
    });
  }
}
