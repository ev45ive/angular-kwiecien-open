import { Injectable } from '@angular/core';
import { HttpParams, HttpClient, HttpBackend } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

export abstract class AuthConfig {
  issuer: string;
  redirectUri: string;
  clientId: string;
  responseType: string;
  scope: string;
}

type TokenData = {
  access_token: string;
  token_type: 'Bearer';
  expires_in: number;
  refresh_token: string;
  scope: string;
};

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  token: TokenData | null = null;

  isAuthenticated = new BehaviorSubject(false)

  http = new HttpClient(this.httpBackend);

  constructor(private config: AuthConfig, private httpBackend: HttpBackend) {
    let tokenData = null;
    try {
      tokenData = JSON.parse(sessionStorage.getItem('token'));
    } catch (e) {}

    if (tokenData) {
      this.updateTokenData(tokenData)
    }

    window.addEventListener('message', (event) => {
      if (event.data.type == 'LOGIN' && event.data.data) {
        this.updateTokenData(event.data.data);
      }
    });
  }

  updateTokenData(tokenData: TokenData) {
    this.token = tokenData;
    sessionStorage.setItem('token', JSON.stringify(tokenData));
    this.isAuthenticated.next(true)
  }

  authorize() {
    const { clientId, responseType, issuer, redirectUri, scope } = this.config;

    // this.logout();

    const p = new HttpParams({
      fromObject: {
        client_id: clientId,
        response_type: responseType,
        redirect_uri: redirectUri,
        scope,
        show_dialog: 'true',
      },
    });

    const url = `${issuer}?${p.toString()}`;
    window.open(url, '_blank', 'width=500,height=500');
  }

  logout() {
    sessionStorage.removeItem('token');
    this.isAuthenticated.next(false)
  }

  refreshToken() {
    return this.http
      .get<TokenData>(`http://localhost:4200/refresh_token`, {
        params: {
          refresh_token: this.token.refresh_token,
        },
      })
      .pipe(
        tap((tokenData) =>
          this.updateTokenData({
            ...this.token,
            ...tokenData,
          })
        )
      );
  }

  getToken() {
    // if (!this.token) {
    //   return this.authorize();
    // }
    return this.token?.access_token;
  }
}
