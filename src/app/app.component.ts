import {
  Component,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
} from '@angular/core';
import { AuthService } from './security/auth.service';

@Component({
  selector: 'app-root, placki.malinowe',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  title = 'angular-open-kwiecien';

  constructor(private auth: AuthService) {}


  login() {
    this.auth.authorize();
  }
}
