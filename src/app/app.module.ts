import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlaylistsModule } from './playlists/playlists.module';
import { ExampleComponent } from './example.component';
import { UsersModule } from './users/users.module';
import { SecurityModule } from './security/security.module';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { CounterComponent } from './views/counter/counter.component';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from 'src/environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { MUSIC_API_CONFIG } from './music-search/services/tokens';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [AppComponent, ExampleComponent, CounterComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    SharedModule,
    SecurityModule.forRoot(environment.authCodeFlowConfig, {
      interceptor: true,
      authTokenUrls:[
        'https://api.spotify.com/'
      ]
    }),
    StoreModule.forRoot(reducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
        strictStateSerializability: true,
        strictActionSerializability: true,
        strictActionWithinNgZone: true,
      },
    }),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
    // PlaylistsModule,// loadChildren lazy loaded feature module
    // MusicSearchModule, // loadChildren lazy loaded feature module
    UsersModule,
    AppRoutingModule,
  ],
  providers: [
    {
      provide: MUSIC_API_CONFIG,
      useValue: {
        url: environment.music_search_api,
      },
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(private app: ApplicationRef) {
    // app.tick()
  }

  // ngDoBootstrap(){
  //   this.app.bootstrap(AppComponent,'app-root')
  //   this.app.bootstrap(AppComponent,'placki.malinowe')
  // }
}
