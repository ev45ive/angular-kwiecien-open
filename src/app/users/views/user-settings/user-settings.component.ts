import { Component, OnInit } from '@angular/core';
import { ICanDeactivate } from '../../guards/user-access.guard';

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.scss']
})
export class UserSettingsComponent implements OnInit, ICanDeactivate {

  hide = false;

  canI = false

  constructor() { }

  canDeactivate(): boolean {
    return this.canI
  }

  ngOnInit(): void {
  }

}
