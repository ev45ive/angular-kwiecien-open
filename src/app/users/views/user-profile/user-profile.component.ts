import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
})
export class UserProfileComponent implements OnInit {
  avatarUrl = '';

  constructor() {}

  ngOnInit(): void {}

  upload(fileChange: FileList) {
    const reader = new FileReader();
    reader.readAsDataURL(fileChange[0]);
    reader.onload = () => {
      this.avatarUrl = reader.result as string;
    };


    var formData = new FormData();

    formData.append("user-id",'123');
    formData.append("avatar-file",fileChange[0]);

    var request = new XMLHttpRequest();
    request.open("POST", "http://foo.com/submitform.php");
    request.send(formData);

  }
}
