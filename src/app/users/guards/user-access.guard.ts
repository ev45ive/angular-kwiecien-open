import { Injectable } from '@angular/core';
import {
  CanActivate,
  CanActivateChild,
  CanDeactivate,
  CanLoad,
  Route,
  UrlSegment,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { delay, skip, switchMap, combineAll } from 'rxjs/operators';
import { AuthService } from 'src/app/security/auth.service';

export interface ICanDeactivate {
  canDeactivate():boolean
}

@Injectable({
  providedIn: 'root',
})
export class UserAccessGuard
  implements
    CanActivate, CanDeactivate<ICanDeactivate> /* , CanActivateChild, CanLoad  */ {
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    const redirect = this.router.createUrlTree(['/playlists'], {});

    // return of(true).pipe(delay(2000));
    // return of(redirect).pipe(delay(2000));

    return this.auth.isAuthenticated.pipe(
      switchMap((loggedIn) => {
        if (loggedIn) {
          return of(true);
        } else {
          this.auth.authorize();
          return this.auth.isAuthenticated.pipe(skip(1));
        }
      })
    );
  }

  constructor(private router: Router, private auth: AuthService) {}

  // canActivateChild(
  //   next: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
  //   return true;
  // }

  canDeactivate(
    component: ICanDeactivate,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {


    return component.canDeactivate();
  }

  // canLoad(
  //   route: Route,
  //   segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
  //   return true;
  // }
}
