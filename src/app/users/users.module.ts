import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UserProfileComponent } from './views/user-profile/user-profile.component';
import { FileUploadDirective } from './shared/file-upload.directive';
import { UserSettingsComponent } from './views/user-settings/user-settings.component';
import { TabsComponent } from './shared/tabs/tabs.component';
import { TabComponent } from './shared/tab/tab.component';
import { TabsNavComponent } from './shared/tabs-nav/tabs-nav.component';
import { TogglerDirective } from './shared/toggler.directive';
import { AdTogglerDirective } from './shared/ad-toggler.directive';
import { AdExampleAComponent } from './shared/ad-example-a/ad-example-a.component';
import { AdExampleBComponent } from './shared/ad-example-b/ad-example-b.component';

@NgModule({
  declarations: [
    UserProfileComponent,
    FileUploadDirective,
    UserSettingsComponent,
    TabsNavComponent,
    TabsComponent,
    TabComponent,
    TogglerDirective,
    AdTogglerDirective,
    AdExampleAComponent,
    AdExampleBComponent,
  ],
  imports: [CommonModule, UsersRoutingModule],
})
export class UsersModule {}
