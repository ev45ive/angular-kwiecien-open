import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserProfileComponent } from './views/user-profile/user-profile.component';
import { UserSettingsComponent } from './views/user-settings/user-settings.component';
import { UserAccessGuard } from './guards/user-access.guard';

const routes: Routes = [
  {
    path: 'user',
    component: UserProfileComponent,
  },
  {
    path: 'user-settings',
    component: UserSettingsComponent,
    canActivate: [UserAccessGuard],
    canDeactivate: [UserAccessGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule {}
