import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdExampleBComponent } from './ad-example-b.component';

describe('AdExampleBComponent', () => {
  let component: AdExampleBComponent;
  let fixture: ComponentFixture<AdExampleBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdExampleBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdExampleBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
