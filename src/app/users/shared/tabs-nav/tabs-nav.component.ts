import { Component, OnInit } from '@angular/core';
import { TabComponent } from 'src/app/users/shared/tab/tab.component';

@Component({
  selector: 'app-tabs-nav',
  templateUrl: './tabs-nav.component.html',
  styleUrls: ['./tabs-nav.component.scss']
})
export class TabsNavComponent implements OnInit {
  tabs: TabComponent[] = []

  constructor() { }

  ngOnInit(): void {
  }

}
