import { Directive, TemplateRef, ViewContainerRef, Input } from '@angular/core';

type Ctx = {
  $implicit: string;
};

@Directive({
  selector: '[appToggler]',
})
export class TogglerDirective {
  @Input('appToggler')
  set value(hide) {

    const ctx = {
      $implicit: ['psa', 'kota'][Math.floor(Math.random() * 2)],
      user:'Ala'
    };

    if (hide) {
      this.vcr.clear();
    } else {
      this.vcr.createEmbeddedView(this.tpl, ctx, 0);
    }
  }

  constructor(
    private tpl: TemplateRef<Ctx>, private vcr: ViewContainerRef) {}
}
