import { Component, OnInit, ViewChild, ContentChild, AfterViewInit } from '@angular/core';
import { TabComponent } from '../tab/tab.component';
import { TabsNavComponent } from 'src/app/users/shared/tabs-nav/tabs-nav.component';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
})
export class TabsComponent implements OnInit {

  @ContentChild(TabsNavComponent, { static: true })
  nav: TabsNavComponent;

  toggle(tab: TabComponent) {
    this.tabs.forEach((tab) => (tab.open = false));
    tab.open = true;
  }

  tabs: TabComponent[] = [];

  ngAfterContentInit() {
    this.nav.tabs = this.tabs;
  }

  constructor() {}

  ngOnInit(): void {}
}
