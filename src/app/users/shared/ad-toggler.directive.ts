import {
  Directive,
  ViewContainerRef,
  Input,
  ComponentFactoryResolver,
  ComponentFactory,
} from '@angular/core';
import { AdExampleAComponent } from './ad-example-a/ad-example-a.component';
import { AdExampleBComponent } from './ad-example-b/ad-example-b.component';

@Directive({
  selector: '[appAdToggler]',
})
export class AdTogglerDirective {
  @Input('appAdToggler')
  set hide(hide) {
    const A = this.cfr.resolveComponentFactory(AdExampleAComponent);
    const B = this.cfr.resolveComponentFactory(AdExampleBComponent);

    const f: ComponentFactory<AdExampleAComponent | AdExampleBComponent> = [
      A,
      B,
    ][Math.floor(Math.random() * 2)];

    this.vcr.clear()
    const cRef = this.vcr.createComponent(f, 0, null, []);

    cRef.instance.title = 'Placki';
  }

  constructor(
    private cfr: ComponentFactoryResolver,
    private vcr: ViewContainerRef
  ) {}
}
