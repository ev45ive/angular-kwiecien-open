import {
  Directive,
  HostListener,
  HostBinding,
  Output,
  EventEmitter,
} from '@angular/core';

@Directive({
  selector: '[appFileUpload]',
  exportAs:'appUpload'
})
export class FileUploadDirective {
  @HostBinding('class.active')
  active: boolean;

  @Output()
  fileChange = new EventEmitter<FileList>();


  @HostListener('document:drag', ['$event'])
  onDrag(event: DragEvent) {
    this.active = true;
  }


  @HostListener('dragenter', ['$event'])
  onDragenter(event: DragEvent) {
    this.active = true;
  }

  @HostListener('dragover', ['$event'])
  onDragover(event: DragEvent) {
    if (event.dataTransfer.types[0] == 'Files') {
      event.preventDefault();
    }
  }

  @HostListener('dragleave', ['$event'])
  onDragleave(event: DragEvent) {
    this.active = false;
  }

  @HostListener('drop', ['$event'])
  onDrop(event: DragEvent) {
    event.preventDefault();

    this.fileChange.emit(event.dataTransfer.files);
  }

  reset(){

  }

  constructor() {}
}
