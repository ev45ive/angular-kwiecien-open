import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdExampleAComponent } from './ad-example-a.component';

describe('AdExampleAComponent', () => {
  let component: AdExampleAComponent;
  let fixture: ComponentFixture<AdExampleAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdExampleAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdExampleAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
