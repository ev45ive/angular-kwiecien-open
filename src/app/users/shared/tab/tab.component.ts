import { Component, OnInit, Input, Optional } from '@angular/core';
import { TabsComponent } from '../tabs/tabs.component';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss'],
})
export class TabComponent implements OnInit {
  @Input() title = '';

  open = false;

  toggle() {
    if (this.container) {
      this.container.toggle(this);
    } else {
      this.open = !this.open;
    }
  }

  constructor(@Optional() private container: TabsComponent) {
    if (this.container) {
      container.tabs.push(this);
    }
  }

  ngOnDestroy() {
    if (this.container) {
      const index = this.container.tabs.indexOf(this);
      this.container.tabs.splice(index, 1);
    }
  }

  ngOnInit(): void {}
}
