import { NgModule } from '@angular/core';
import { Routes, RouterModule, NoPreloading, RouterPreloader, PreloadAllModules } from '@angular/router';
import { PlaylistsViewComponent } from './playlists/views/playlists-view/playlists-view.component';
import { CounterComponent } from './views/counter/counter.component';
import { ReduxPlaylistsViewComponent } from './playlists/views/redux-playlists-view/redux-playlists-view.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'playlists',
    pathMatch: 'full',
  },
  {
    path: 'counter',
    component: CounterComponent,
  },
  {
    path: 'search',
    loadChildren: () =>
      import('./music-search/music-search.module') //
        .then((m) => m.MusicSearchModule),
  },
  {
    path: 'playlists',
    loadChildren: () =>
      import('./playlists/playlists.module') //
        .then((m) => m.PlaylistsModule),
  },
  {
    path: '**',
    // component: PageNotFoundComponent
    redirectTo: 'playlists',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      enableTracing: true,
      useHash: false,
      // initialNavigation:true,
      // paramsInheritanceStrategy:'emptyOnly',
      preloadingStrategy:NoPreloading,
      // preloadingStrategy:PreloadAllModules,
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
