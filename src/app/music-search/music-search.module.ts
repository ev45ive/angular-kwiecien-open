import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusicSearchRoutingModule } from './music-search-routing.module';
import { MusicSearchComponent } from './views/music-search/music-search.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { environment } from 'src/environments/environment';
import { MUSIC_API_CONFIG } from './services/tokens';
import { ReactiveFormsModule } from '@angular/forms';
import { SyncedComponent } from './views/synced/synced.component';
import { StoreModule } from '@ngrx/store';
import * as fromMusicSearch from '../reducers/music-search.reducer';
import { ReduxSearchComponent } from './views/redux-search/redux-search.component';
import { EffectsModule } from '@ngrx/effects';
import { SearchEffects } from './search.effects';
import { AlbumsSearchService } from './services/albums-search.service';
// import { AlbumsSearchService } from './services/albums-search.service';

@NgModule({
  declarations: [
    MusicSearchComponent,
    SearchFormComponent,
    SearchResultsComponent,
    AlbumCardComponent,
    SyncedComponent,
    ReduxSearchComponent,
  ],
  imports: [
    CommonModule,
    MusicSearchRoutingModule,
    ReactiveFormsModule,
    StoreModule.forFeature(
      fromMusicSearch.musicSearchFeatureKey,
      fromMusicSearch.reducer
    ),
    EffectsModule.forFeature([SearchEffects]),
  ],
  providers: [
    // AlbumsSearchService,
    // {
    //   provide:HttpClient, useClass: MyCustomHttpClient
    // },
    {
      provide: MUSIC_API_CONFIG,
      useValue: {
        url: environment.music_search_api,
      },
    },
    // {
    //   provide: AlbumsSearchService,
    //   useFactory(config){
    //     return new AlbumsSearchService(config)
    //   },
    //   deps:[MUSIC_API_CONFIG]
    // },
    // {
    //   provide: AlbumsSearchService,
    //   useClass: SpotifyAlbumsSearchService,
    //   // deps:[ALTERNATIVE_MUSIC_API_CONFIG]
    // },
    // {
    //   provide:AlbumsSearchService,
    //   useClass:AlbumsSearchService
    // },
    // {
    //   provide:SingleSearchService,
    //   useExisting: AlbumsSearchService
    // },
    // AlbumsSearchService,
  ],
})
export class MusicSearchModule {}
