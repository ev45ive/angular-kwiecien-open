import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import {
  FormGroup,
  FormControl,
  FormArray,
  FormBuilder,
  Validators,
  Validator,
  ValidatorFn,
  ValidationErrors,
  AbstractControl,
  AsyncValidatorFn,
} from '@angular/forms';
import {
  filter,
  debounceTime,
  distinctUntilChanged,
  mapTo,
  map,
  withLatestFrom,
} from 'rxjs/operators';
import { Observable, of, combineLatest } from 'rxjs';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchFormComponent implements OnInit {
  @Output() searchChange = new EventEmitter<string>();

  @Input('query')
  set query(q) {
    (this.searchForm.get('query') as FormControl).setValue(q, {
      emitEvent: false,
    });
  }

  searchForm = this.fb.group(
    {
      // x: new FormControl('',{   updateOn:'blur'  }),
      query: [
        '',
        [Validators.required, Validators.minLength(3)],
        [asyncCensor],
      ],
      type: ['album'],
    },
    {
      validators: [],
      asyncValidators: [],
    }
  );
  formSubmitted: boolean;

  constructor(private fb: FormBuilder, private cdr: ChangeDetectorRef) {
    (window as any).form = this.searchForm;

    const queryField = this.searchForm.get('query');

    const values = queryField.valueChanges;
    const status = queryField.statusChanges;

    // status.subscribe(() => this.cdr.markForCheck());

    const valid = status.pipe(
      filter((s) => s == 'VALID'),
      mapTo(true)
    );

    // const search = combineLatest(valid, values).pipe(map(([valid, value]) => value));

    const search = valid.pipe(
      withLatestFrom(values),
      map(([valid, value]) => value),
      distinctUntilChanged()
      // debounceTime(400),
    );

    // search.subscribe((q) => console.log(q));
    search.subscribe((q) => this.search(q));
  }

  search(q: string) {
    this.formSubmitted = true;
    this.searchChange.emit(q);
  }

  ngOnInit(): void {}
}

const censor: ValidatorFn = (
  control: AbstractControl
): ValidationErrors | null => {
  const badword = 'batman';

  return ('' + control.value).includes(badword)
    ? {
        censor: { badword },
      }
    : null;
};

const asyncCensor: AsyncValidatorFn = (
  control: AbstractControl
): Observable<ValidationErrors | null> => {
  // return this.http.post('/validate',control.value).pipe(map(res=> errors | null ))
  // return of(result);
  const result = censor(control);
  // console.log('Run censor');

  return new Observable((observer) => {
    // console.log('Subscribe');

    const handle = setTimeout(() => {
      // console.log('Result');
      observer.next(result);
      observer.complete();
    }, 1000);

    // observer.add(/* teardown */)

    return /* teardown */ () => {
      clearTimeout(handle);
      // console.log('Cancelled');
    };
  });
};
