import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MusicSearchComponent } from './views/music-search/music-search.component';
import { SyncedComponent } from './views/synced/synced.component';
import { ReduxSearchComponent } from './views/redux-search/redux-search.component';

const routes: Routes = [
  {
    path: '',
    // component: MusicSearchComponent,
    component: ReduxSearchComponent,
  },
  {
    path: 'sync',
    component: SyncedComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MusicSearchRoutingModule {}
