import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SyncedComponent } from './synced.component';

describe('SyncedComponent', () => {
  let component: SyncedComponent;
  let fixture: ComponentFixture<SyncedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SyncedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SyncedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
