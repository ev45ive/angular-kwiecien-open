import {
  Component,
  OnInit,
  Inject,
  ChangeDetectionStrategy,
} from '@angular/core';
import { Album, AlbumsResponse } from 'src/app/models/album';
import { AlbumsSearchService } from '../../services/albums-search.service';
import {
  tap,
  takeUntil,
  multicast,
  refCount,
  share,
  shareReplay,
} from 'rxjs/operators';
import {
  Subscription,
  Subject,
  ConnectableObservable,
  ReplaySubject,
} from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-music-search',
  templateUrl: './music-search.component.html',
  styleUrls: ['./music-search.component.scss'],
})
export class MusicSearchComponent implements OnInit {
  notifications$ = this.service.notifications;
  query$ = this.service.query;

  results$ = this.service.results;

  search(q: string) {
    this.service.search(q);
  }

  constructor(private service: AlbumsSearchService) {}

  ngOnInit(): void {}
}
