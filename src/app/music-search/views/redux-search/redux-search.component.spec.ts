import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReduxSearchComponent } from './redux-search.component';
import { Store, StoreModule } from '@ngrx/store';

describe('ReduxSearchComponent', () => {
  let component: ReduxSearchComponent;
  let fixture: ComponentFixture<ReduxSearchComponent>;
  let store: Store<any>;

  beforeEach(async() => {
    TestBed.configureTestingModule({
      imports: [ StoreModule.forRoot({}) ],
      declarations: [ ReduxSearchComponent ]
    });

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReduxSearchComponent);
    component = fixture.componentInstance;
    store = TestBed.get<Store>(Store);

    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
