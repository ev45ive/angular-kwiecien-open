import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from 'src/app/reducers';
import { AlbumSearch } from 'src/app/actions/search.actions';
import {
  albumSearchResultsSelector,
  albumSearchQuerySelector,
} from 'src/app/selectors/search.selectors';
import { Router, ActivatedRoute } from '@angular/router';
import { map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-redux-search',
  templateUrl: './redux-search.component.html',
  styleUrls: ['./redux-search.component.css'],
})
export class ReduxSearchComponent implements OnInit {
  results = this.store.select(albumSearchResultsSelector);
  query = this.store.select(albumSearchQuerySelector);

  constructor(
    private store: Store<State>,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.queryParamMap
      .pipe(
        map((paramMap) => paramMap.get('query')),
        filter((q) => !!q)
      )
      .subscribe((query) => this.store.dispatch(AlbumSearch({ query })));

    // this.route.queryParamMap.subscribe((paramMap) => {
    //   const query = paramMap.get('query');
    //   if (query) {
    //     this.store.dispatch(AlbumSearch({ query }));
    //   }
    // });
  }

  search(query: string) {
    this.router.navigate(
      [
        /* '/search' */
        /* '..' */
        /* './sibling' */
      ],
      {
        relativeTo: this.route,
        replaceUrl: true,
        queryParams: {
          query,
        },
      }
    );
  }
}
