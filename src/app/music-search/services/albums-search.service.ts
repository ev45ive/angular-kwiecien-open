import { Injectable, Inject } from '@angular/core';
import { AlbumsResponse } from 'src/app/models/album';
import { MUSIC_API_CONFIG } from './tokens';

import { HttpClient } from '@angular/common/http';

import {
  map,
  distinctUntilChanged,
  mergeMap,
  switchMap,
  startWith,
  filter,
  mergeAll,
} from 'rxjs/operators';
import {
  ReplaySubject,
  BehaviorSubject,
  Observable,
  Subject,
  of,
  merge,
} from 'rxjs';
import { MusicSearchModule } from '../music-search.module';

const select = <T, R>(selector: (value: T, index: number) => R) => (
  source: Observable<T>
): Observable<R> => source.pipe(map(selector), distinctUntilChanged());

enum ActionTypes {
  SEARCH = 'SEARCH',
  NEXT_PAGE = 'NEXT_PAGE',
  RESET = 'RESET',
}

  @Injectable({
    providedIn: 'root',
    // providedIn: MusicSearchModule
})
export class AlbumsSearchService {
  initialState = {
    results: [
      {
        id: '123',
        name: 'Service test 123',
        artists: [],
        images: [
          {
            url: 'https://www.placecage.com/c/500/500',
          },
        ],
      },
      {
        id: '123',
        name: 'test 234',
        artists: [],
        images: [
          {
            url: 'https://www.placecage.com/c/500/500',
          },
        ],
      },
      {
        id: '123',
        name: 'test 345',
        artists: [],
        images: [
          {
            url: 'https://www.placecage.com/c/500/500',
          },
        ],
      },
      {
        id: '123',
        name: 'test 123',
        artists: [],
        images: [
          {
            url: 'https://www.placecage.com/c/500/500',
          },
        ],
      },
    ],
    query: 'batman',
  };

  notifications = new ReplaySubject<Error | null>(3, 30 * 1000);
  private stateChange = new BehaviorSubject(this.initialState);

  query = this.stateChange.pipe(select((s) => s.query));
  results = this.stateChange.pipe(select((s) => s.results));

  actions = new Subject<{ type: ActionTypes; payload: any }>();

  constructor(
    @Inject(MUSIC_API_CONFIG) public config: { url: string },
    private http: HttpClient
  ) {
    (window as any).state = this.stateChange;

    this.actions
      .pipe(
        mergeMap((action) => [
          of(action).pipe(
            filter(({ type }) => type == ActionTypes.SEARCH),
            switchMap(({ payload }) => {
              return this.fetchResults(payload).pipe(
                map((results) => ({ results }))
              );
            })
          ),

          of(action).pipe(filter(({ type }) => type == ActionTypes.NEXT_PAGE)),
        ]),
        mergeAll()
      )
      .subscribe(console.log);

    // .pipe(
    //   mergeMap((action) => {
    //     switch (action.type) {
    //       case ActionTypes.SEARCH:
    //         return merge(
    //           of({ query: action.payload }),
    //           this.fetchResults(action.payload).pipe(
    //             map((results) => ({ results }))
    //           )
    //         );

    //       case ActionTypes.NEXT_PAGE:

    //       case ActionTypes.RESET:
    //       default:
    //         return of(this.stateChange.getValue());
    //     }
    //   })
    // )
    // .subscribe({
    //   next: (state) => this.updateState(state),
    // });
  }

  search(query = 'batman') {
    this.actions.next({
      type: ActionTypes.SEARCH,
      payload: query,
    });
  }

  fetchResults(query: string) {
    return this.http
      .get<AlbumsResponse>(this.config.url, {
        params: {
          type: 'album',
          q: query,
        },
      })
      .pipe(map((resp) => resp.albums.items));
  }

  private updateState(state): void {
    return this.stateChange.next({
      ...this.stateChange.getValue(),
      ...state,
    });
  }

  // getResults() {
  //   return this.stateChange.pipe(map((s) => s.results));
  // }
}

// https://github.com/manfredsteyer/angular-oauth2-oidc
// https://www.baeldung.com/rest-api-spring-oauth2-angular

/* CORS:
Req:
origin: http://localhost:4200
sec-fetch-dest: empty
sec-fetch-mode: cors
sec-fetch-site: cross-site

Res:
access-control-allow-credentials: true
access-control-allow-headers: Accept, App-Platform, Authorization, Content-Type, Origin, Retry-After, Spotify-App-Version, X-Cloud-Trace-Context
access-control-allow-methods: GET, POST, OPTIONS, PUT, DELETE, PATCH
access-control-allow-origin: *
access-control-max-age: 604800
*/
