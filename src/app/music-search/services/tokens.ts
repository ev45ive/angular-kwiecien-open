import { InjectionToken } from '@angular/core';

export const MUSIC_API_CONFIG = new InjectionToken<{
  url: string;
}>('Music Api URL');
