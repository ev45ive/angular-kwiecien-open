import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AlbumsSearchService } from './services/albums-search.service';
import { mergeMap, map, catchError, switchMap } from 'rxjs/operators';
import { EMPTY, of } from 'rxjs';
import {
  AlbumSearch,
  AlbumSearchSuccess,
  AlbumSearchFailure,
} from '../actions/search.actions';

@Injectable()
export class SearchEffects {
  constructor(
    private actions$: Actions,
    private service: AlbumsSearchService
  ) {}

  searchAlbums$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AlbumSearch /* ,AlbumSearch */),
      switchMap((action) =>
        this.service.fetchResults(action.query).pipe(
          map(
            (results) => AlbumSearchSuccess({ results }),
            catchError((error) => of(AlbumSearchFailure({ error })))
          )
        )
      )
    )
  );
}
