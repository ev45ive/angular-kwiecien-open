// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { AuthConfig } from 'src/app/security/auth.service';


export const environment = {
  production: false,
  music_search_api:'https://api.spotify.com/v1/search',

  authCodeFlowConfig: {
    // Url of the Identity Provider
    issuer: 'https://accounts.spotify.com/authorize',

    // URL of the SPA to redirect the user to after login
    // redirectUri:  'http://localhost:4200/auth_callback.html',
    redirectUri:  'http://localhost:4200/auth_callback',

    // The SPA's id. The SPA is registerd with this id at the auth-server
    // clientId: 'server.code',
    clientId: '70599ee5812a4a16abd861625a38f5a6',

    // Just needed if your auth server demands a secret. In general, this
    // is a sign that the auth server is not configured with SPAs in mind
    // and it might not enforce further best practices vital for security
    // such applications.
    // dummyClientSecret: 'secret',

    responseType: 'code',
    // responseType: 'token',

    // set the scope for the permissions the client should request
    // The first four are defined by OIDC.
    // Important: Request offline_access to get a refresh token
    // The api scope is a usecase specific one
    scope: 'user-read-private user-read-email playlist-read-private',

    showDebugInformation: true,

    // Not recommented:
    // disablePKCI: true,
  } as AuthConfig
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
