# Git
git clone https://bitbucket.org/ev45ive/angular-kwiecien-open.git
cd angular-kwiecien-open
npm i 
npm start 

# Instalacja
npm install --global @angular/cli 
npm i -g @angular/cli 
/c/Users/ev45i/AppData/Roaming/npm

# Angular CLI
ng help
ng.cmd help // powershell

# Tworzymy aplikacje
cd ..
ng new angular-open-kwiecien

? Would you like to add Angular routing? Yes
? Which stylesheet format would you like to use? SCSS   [ https://sass-lang.com/documentation/syntax#scss ]

cd angular-open-kwiecien

# Lokalny server
ng serve
ng s -o 
npm start

# Music App

ng g m playlists --routing -m app --dry-run

ng g m playlists --routing -m app

<!-- Pages / Navigation Views -->
ng g c playlists/views/playlists-view

<!-- Presenters / Visual Lego Blocks  -->
ng g c playlists/components/items-list
ng g c playlists/components/list-item
ng g c playlists/components/playlist-details
ng g c playlists/components/playlist-form

<!-- Containers / Logical Views -->
<!-- 
ng g c playlists/containers/my-playlists -> items-list
ng g c playlists/containers/recent-playlists -> items-list
-->

# Boostrap 
npm install bootstrap --save

# Shared
ng g m shared -m playlists

ng g d shared/highlight --export

# File upload

ng g m users --routing -m app
ng g c users/views/user-profile
ng g d users/shared/file-upload

# Tabs
ng g c users/views/user-settings
ng g c users/shared/tabs
ng g c users/shared/tab


# Music Search

ng g m music-search --routing -m app

ng g c music-search/views/music-search

ng g c music-search/components/search-form
ng g c music-search/components/search-results
ng g c music-search/components/album-card

ng g i models/album

ng g s music-search/services/albums-search

https://developer.spotify.com/documentation/web-api/reference/search/search/#fields-reference


.row>.col>app-search-form^.col>app-search-results


https://getbootstrap.com/docs/4.4/components/input-group/#button-addons
https://github.com/manfredsteyer/angular-oauth2-oidc
https://getbootstrap.com/docs/4.4/components/card/#card-groups


# Oauth
https://www.baeldung.com/spring-security-openid-connect

placki@placki.com
******

# Kerberos
https://newspark.nl/authentication-with-angular/

npm i --save express @types/express got ts-node


# Websockets

 https://rxjs-dev.firebaseapp.com/api/webSocket/webSocket

# RxjS
http://rxmarbles.com
https://www.learnrxjs.io/


# NgRx
https://ngrx.io/guide/store/install

npm install @ngrx/{store,effects,entity,store-devtools} --save

# Store
ng add @ngrx/store
ng add @ngrx/store --minimal false

ng generate @ngrx/schematics:

# Reducer
ng generate @ngrx/schematics:reducer Counter --reducers reducers/index.ts --group true

? Should we add success and failure actions to the reducer? Yes
? Do you want to use the create function? Yes

CREATE src/app/reducers/counter.reducer.spec.ts (332 bytes)
CREATE src/app/reducers/counter.reducer.ts (235 bytes)
UPDATE src/app/reducers/index.ts (519 bytes)

# Actions
ng generate @ngrx/schematics:action Counter --group true
? Should we generate success and failure actions? Yes
? Do you want to use the create function? Yes
CREATE src/app/actions/counter.actions.spec.ts (211 bytes)
CREATE src/app/actions/counter.actions.ts (356 bytes)

# Feature reducers

ng generate @ngrx/schematics:reducer MusicSearch --reducers reducers/index.ts --group true --module music-search --dry-run true

ng g @ngrx/schematics:container music-search/views/redux-search --state reducers/index.ts 


ng generate @ngrx/schematics:action MusicSearch --group true --module music-search --dry-run true

# Effects
ng generate @ngrx/schematics:effect music-search/Search -m music-search/music-search.module.ts 


# Feature schematics

ng generate @ngrx/schematics:store playlists/Playlists -m playlists/playlists.module.ts

ng generate @ngrx/schematics:feature playlists/Playlist --group --reducers reducers/index.ts -m playlists/playlists.module.ts --api true

# Selectors
<!-- https://github.com/reduxjs/reselect -->

ng generate @ngrx/schematics:selector search-results --group 

# Playlists

ng g @ngrx/schematics:container playlists/views/redux-playlists-view --state ../../reducers/index.ts

# Routing - Containrs
https://marketplace.visualstudio.com/items?itemName=cyrilletuzi.angular-schematics

ng g @ngrx/schematics:container playlists/containers/playlists-list --changeDetection OnPush --state ../../reducers/index.ts

# Lazy loading / module types
https://angular.io/guide/module-types

# Preloading
https://web.dev/en/route-preloading-in-angular/

# Guards

ng g guard users/guards/user-access --implements CanActivate --implements CanActivateChild --implements CanDeactivate --implements CanLoad
CREATE src/app/users/guards/user-access.guard.spec.ts (362 bytes)
CREATE src/app/users/guards/user-access.guard.ts (1152 bytes)

# Library
<!-- https://github.com/ng-packagr/ng-packagr -->
https://angular.io/guide/creating-libraries

ng g library nav-loader --prefix war

# Monorepo / Workspaces
https://nx.dev/angular

# Npm PRivatge Proxy
https://verdaccio.org/docs/en/what-is-verdaccio
https://verdaccio.org/docs/en/installation


# Build 
ng build 

ng build --env=mojenvironemt

ng build --aot --source-map

ng build --prod --source-map


# Bundle size analysis
npm i -g source-map-explorer
source-map-explorer /dist/aplikacja/main.js
source-map-explorer dist/angular-open-kwiecien/vendor-es2015.js

# Ankieta
https://tiny.pl/7qc86 

# Kontakt
https://www.linkedin.com/in/mateuszkulesza/ <- 2 english sentences reccomendation, Much appreciated! :D 


# PDF in browser
pdf.js

http://pdfmake.org/playground.html


