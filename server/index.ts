import express from 'express';
import got from 'got';
import path from 'path';

const app = express();

const clientId = '70599ee5812a4a16abd861625a38f5a6';
const clientSecret = '624d68ed7e3d4db685957481bf8a2b1c';

let code = '';

app.use(
  express.static(path.join(__dirname, '..','dist', 'angular-open-kwiecien'), {})
);

app.get('/auth_callback', (req, res) => {
  code = req.query['code'];

  got
    .post('https://accounts.spotify.com/api/token', {
      headers: {
        // Accept: 'application/json',
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      form: {
        grant_type: 'authorization_code',
        code: code,
        redirect_uri: 'http://localhost:4200/auth_callback',
        client_id: clientId,
        client_secret: clientSecret,
      },
      responseType: 'json',
    })
    .then(
      (resp) => {
        console.log(resp.body);
        res.send(`<html><body>
        <h1>Logging you ...</h1>
        <script>
          window.opener.postMessage(${JSON.stringify(
            {
              type: 'LOGIN',
              data: resp.body,
            },
            null,
            2
          )},'*');
          window.close()
        </script>
            </body></html>
        `);
      },
      (resp) => {
        console.log(resp);
        res.send(resp.error);
      }
    );
});

app.get('/refresh_token', (req, res) => {
  const refresh_token = req.query.refresh_token;

  got
    .post('https://accounts.spotify.com/api/token', {
      headers: {
        // Accept: 'application/json',
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      form: {
        grant_type: 'refresh_token',
        code: code,
        refresh_token,
        client_id: clientId,
        client_secret: clientSecret,
      },
      responseType: 'json',
    })
    .then((resp) => {
      res.send(resp.body);
    });
});

const PORT = 4200;
app.listen(PORT, '0.0.0.0', () => {
  console.log('listetning');
});
