import { TestBed } from '@angular/core/testing';

import { NavLoaderService } from './nav-loader.service';

describe('NavLoaderService', () => {
  let service: NavLoaderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NavLoaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
