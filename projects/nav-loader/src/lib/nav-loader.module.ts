import { NgModule } from '@angular/core';
import { NavLoaderComponent } from './nav-loader.component';

import { NgxLoadingModule } from 'ngx-loading';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [NavLoaderComponent],
  imports: [
    NgxLoadingModule.forRoot({}),
    CommonModule,
    RouterModule.forChild([]),
  ],
  exports: [NavLoaderComponent, NgxLoadingModule],
})
export class NavLoaderModule {}
