import { Component, OnInit } from '@angular/core';
import { filter, map, distinctUntilChanged } from 'rxjs/operators';
import {
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError,
  Router,
} from '@angular/router';

@Component({
  selector: 'lib-nav-loader',
  template: `
    <div>
      <ng-content></ng-content>
      <ngx-loading
        [show]="isLoading | async"
        [template]="customLoadingTemplate"
      >
      </ngx-loading>
    </div>

    <ng-template #customLoadingTemplate>
      <div class="custom-class">
        <h3>
          Loading...
        </h3>
      </div>
    </ng-template>

    <style>
      .custom-class {
        margin: 50% auto;
        display: block;
        width: 100px;
      }
    </style>
  `,
  styles: [
    `
      :host {
        display: block;
      }
    `,
  ],
})
export class NavLoaderComponent implements OnInit {
  // isLoading = new BehaviorSubject(true);
  isLoading = this.router.events.pipe(
    filter(
      (e) =>
        e instanceof NavigationStart ||
        e instanceof NavigationEnd ||
        e instanceof NavigationCancel ||
        e instanceof NavigationError
    ),
    map((e) => {
      if (e instanceof NavigationStart) {
        return true;
      } else {
        return false;
      }
    }),
    distinctUntilChanged()
  );

  constructor(private router: Router) {}

  ngOnInit(): void {}
}
